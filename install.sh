#!/usr/bin/env bash

# Install OS Dependencies (Debian)
apt install --assume-yes \
  sshpass \
  python3-pip \
  python3-venv

# Create a Python Environment
python3 -m venv .venv

# Install Python Dependencies
. .venv/bin/activate ;\
    pip3 install -r requirements.txt ;\
